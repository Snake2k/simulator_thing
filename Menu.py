# -*- coding: cp1252 -*-
import pygame, sys
from pygame.locals import *

black 	= (0	,0		,0)
red		= (255	,0		,0)
green	= (0	,255	,0)
yellow	= (255	,255	,0)
blue	= (0	,0		,255)
pink	= (255	,0		,255)
cyan	= (0	,255	,255)
white 	= (255	,255	,255)
pygame.font.init()

class Button (object):
	def __init__(self,x,y,w,l,num,text):
		self.pos =(x,y)
		self.width = w
		self.length = l
		self.text = text
		self.font = pygame.font.SysFont("Calibri", 15)
		self.label = self.font.render(self.text, 1, black)
		self.num = num
	def pressed(self, mouse):
		if mouse[0] > self.pos[0] and mouse[1] > self.pos[1] and mouse[0] < self.pos[0] + self.width and mouse[1] < self.pos[1] + self.length:
			from Main import selmen
			selmen(self.num)
		else: return False
	
	def draw(self):
		from Main import MainWindow
		pygame.draw.rect(MainWindow, blue, (self.pos[0],self.pos[1],self.width,self.length),0)
		MainWindow.blit(self.label, (self.pos[0],self.pos[1]))