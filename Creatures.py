import pygame
import random
class Squwabble (object):
	def __init__(self,tile,pop,food):
		self.name = "Squwabble"
		self.pos = tile.pos
		self.pop = pop
		self.colour = (255,255,255)
		self.tile = tile
		self.food = food
		self.tile.occupants.append(self)
	def test(self):
		c = 0
		for cand in self.tile.next:
			if not cand.terrain == 'water':
				c += 1
		if c == 0:
			self.death()
	def step (self):
		if self.pop <= 1:
			self.death()
		else:
			max = 0
			land =  []
			for cand in self.tile.next:
				if not cand.terrain == 'water':
					if cand.food > max:
						land = []
						land.append(cand)
						max = cand.food
					elif cand.food == max:
						land.append(cand)
			tile = land[random.randint(0,len(land)-1)]
			
			if self.food/self.pop > 1:
				self.pop += random.randint (0,3)
			else:
				self.pop -= random.randint (0,3)
				if random.randint (0,100) == 0:
					from Main import creat
					creat.append(Squwabble(self.tile,self.pop/2,self.food/2))
					self.pop -= self.pop/2	
					self.food -= self.food/2
			from Map import Track
			self.tile.tracks.append(Track(self.pop,self.tile,tile))
			self.tile.draw()
			self.tile.occupants.remove(self)
			self.tile = tile
			self.tile.occupants.append(self)
			self.food += self.tile.food
			self.tile.food = 0
			self.food = self.food - self.pop*.2	
			from Main import MainWindow
			pygame.draw.rect(MainWindow,self.colour,self.tile.rect,0)
			pygame.display.update(self.tile.rect)
			if self.pop <= 1:
				self.death()
	def death (self):
		self.tile.draw()
		pygame.display.update(self.tile.rect)
		self.tile.occupants.remove(self)
		from Main import creat
		if self in creat:
			creat.remove(self)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~End of Squwabble~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class Braussian_Spider (object):
	def __init__(self,tile,pop,food):
		self.name = "Braussian Spider"
		self.pos = tile.pos
		self.pop = pop
		self.colour = (0,0,0)
		self.tile = tile
		self.food = food
		self.tile.occupants.append(self)

	def test(self):
		c = 0
		for cand in self.tile.next:
			if not cand.terrain == 'water':
				c += 1
		if c == 0:
			self.death()
			
	def step (self):
		land =  []
		if self.pop <= 0:
			self.death()
		else:
			tile = None
			if len(self.tile.tracks) == 0:
				for cand in self.tile.next:
					if "Squwabble" in cand.occupants:
						tile = cand
					elif not cand.terrain == 'water':
						land.append(cand)
				if tile == None:
					tile = land[random.randint(0,len(land)-1)]
			else:
				opt = max (self.tile.tracks,key=lambda track:track.mag)
				self.tile.tracks.remove(opt)
				tile = opt.to	
			if self.food/self.pop < .5:
				self.pop -= random.randint (0,2)
			self.tile.draw()
			self.tile.occupants.remove(self)
			self.tile = tile
			self.tile.occupants.append(self)
			if len(self.tile.occupants) > 1:
				search = random.randint(0,len(self.tile.occupants)-1)
				if self.tile.occupants[search].name == "Squwabble":
					self.food += self.tile.occupants[search].pop*5
					self.pop += random.randint(0,self.tile.occupants[search].pop)/2
					if self.pop > 8:
						if random.randint (0,100) == 0:
							from Main import carn
							carn.append(Braussian_Spider(self.tile,self.pop/2,self.food/2))
							self.pop -= self.pop/2		
							self.food -= self.food/2
					self.tile.occupants[search].death()
			self.food -= (self.pop)*0.05
			from Main import MainWindow
			pygame.draw.rect(MainWindow,self.colour,self.tile.rect,0)
			pygame.display.update(self.tile.rect)
			if self.pop <= 0:
				self.death()
	def death(self):
		self.tile.draw()
		pygame.display.update(self.tile.rect)
		self.tile.occupants.remove(self)
		from Main import carn
		carn.remove(self)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~End of Braussian Spiders~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~