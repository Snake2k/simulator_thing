import pygame
import random
import perlin
import math
global map_size
black 	= (0	,0		,0)
red		= (255	,0		,0)
lgreen	= (0	,255	,0)
green	= (0	,155	,0)
dgreen	= (0	,55		,0)
yellow	= (255	,255	,0)
blue	= (0	,0		,255)
pink	= (255	,0		,255)
cyan	= (0	,255	,255)
white 	= (255	,255	,255)
global size
size = 5
class Track (object):
	def __init__(self,mag,fro,to):
		self.mag = mag
		self.fro = fro
		self.to = to
class Tile (object):
	def __init__(self,x,y,hg):
		global size
		self.colour = white
		self.pos = [x,y]
		self.rect = (self.pos[0]*size ,self.pos[1]*size ,size ,size )
		self.heigth = round(hg,0)
		self.food = 1
		self.output = 2
		self.movcost = 1
		self.fscore = 1 # h score(estimated cost)
		self.next = []	# nodes next to this one go in heres
		self.tracks = [] #instances who have left a mark in the tile are in here
		self.occupants = []
		
		#self.text = str(self.heigth)
		#self.font = pygame.font.SysFont("Calibri", 14)
		
	def set_tile(self):
		global map_size
		from Main import map
		for n in range(8):
			self.next.append(None)
		self.next[0] = map[self.pos[0]-1][self.pos[1]] 			#left
		self.next[1] = map[self.pos[0]][self.pos[1]-1] 			#up
		self.next[4] = map[self.pos[0]-1][self.pos[1]-1]		#upleft

		if not self.pos[0] == map_size[0]-1 and not self.pos[1] == map_size[1]-1:
			self.next[2] = map[self.pos[0]+1][self.pos[1]] 		#rigth	
			self.next[3] = map[self.pos[0]][self.pos[1]+1] 		#down
			self.next[5] = map[self.pos[0]+1][self.pos[1]-1]	#uprigth		# 4 1 5																									
			self.next[6] = map[self.pos[0]+1][self.pos[1]+1]	#downrigth		# 0 x 2
			self.next[7] = map[self.pos[0]-1][self.pos[1]+1]	#downleft		# 7 3 6
		else:
			if self.pos[0] == map_size[0]-1:
				self.next[2] = map[0][self.pos[1]] 			#rigth	
				self.next[5] = map[0][self.pos[1]-1]			#uprigth																									
				if self.pos[1] == map_size[1]-1:
					self.next[6] = map[0][0]					#downrigth
					self.next[3] = map[self.pos[0]][0] 		#down
					self.next[7] = map[self.pos[0]-1][0]		#downleft
				else:
					self.next[3] = map[self.pos[0]][self.pos[1]+1] 		#down
					self.next[6] = map[0][self.pos[1]+1]				#downrigth	
					self.next[7] = map[self.pos[0]-1][self.pos[1]+1]	#downleft

			elif self.pos[1] == map_size[1]-1:
				self.next[3] = map[self.pos[0]][0]				#down
				self.next[7] = map[self.pos[0]-1][0]			#downleft	
				self.next[6] = map[self.pos[0]+1][0]			#downrigth
				self.next[2] = map[self.pos[0]+1][self.pos[1]] 	#rigth	
				self.next[5] = map[self.pos[0]+1][self.pos[1]-1]#uprigth
				
		if self.heigth < 0:
			self.RGB =  (0,0,1)
			self.terrain = 'water'
		else:
			self.RGB = (0,1,0)
			self.terrain = 'grass'
			#self.RGB = (1/2.,1/1.5,1)
			#self.terrain = 'dirt'
			
	def draw(self):
		from Main import MainWindow
		pygame.draw.rect(MainWindow,self.colour,self.rect,0)
		if self.terrain == 'shrubs':
			pygame.draw.circle(MainWindow, (40,110,40),(self.rect[0]+self.rect[2]/2,self.rect[1]+self.rect[2]/2),self.rect[2]/2, 0)
		if self.terrain == 'woods':
			pygame.draw.circle(MainWindow, (20,55,20),(self.rect[0]+self.rect[2]/2,self.rect[1]+self.rect[2]/2),self.rect[2]/2, 0)
		pygame.display.update(self.rect)
		#self.label = self.font.render(str(self.food), 1, black)
		#MainWindow.blit(self.label, (self.pos[0]*10,self.pos[1]*10))
	
	def step(self):
		if not self.terrain == 'water':
			self.food += self.output
			while(len(self.tracks)>10):
				self.tracks.pop(0)
			#from Main import MainWindow
			#pygame.draw.rect(MainWindow,self.colour,self.rect,0)
			#self.label = self.font.render(str(self.food), 5, black)
			#MainWindow.blit(self.label, (self.pos[0]*40,self.pos[1]*40))
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~tile End~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

			
def set_colour(t):
	t.heigth = round(t.heigth,0)
	if t.heigth > 255:
		t.heigth = 255
	if t.heigth < -255:
		t.heigth = -255
	if t.terrain == 'dirt':
		R = 200-t.RGB[0]*math.fabs(int(t.heigth/2.55))
		G = 170-t.RGB[1]*math.fabs(int(t.heigth/2.04))
		B = 140-t.RGB[2]*math.fabs(int(t.heigth/1.96))
	else:
		if t.RGB[0] == 0:				#Ugly and stupid as fuck, but I could not come up with anything smarter at 1AM
			R = 0
		else:
			R = 255-t.RGB[0]*math.fabs(int(t.heigth))
		if t.RGB[1] == 0:
			G = 0
		else:
			G = 255-t.RGB[1]*math.fabs(int(t.heigth))
		if t.RGB[2] == 0:
			B = 0
		else:
			B = 255-t.RGB[2]*math.fabs(int(t.heigth))
	t.colour =(R,G,B)

def set_sea(map):
	t = 0
	global map_size
	nt = map_size[0]*map_size[1]
	for array in map:
		for tile in array:
			t += tile.heigth
	if t/nt < 0:
		for array in map:
			for tile in array:
				tile.heigth += (t/nt)+10
	else:
		for array in map:
			for tile in array:
				tile.heigth -= (t/nt)-10
		
def erosion(tile):
	t = 0
	for next in tile.next:
		t += next.heigth
	t = t/8
	if t < tile.heigth and tile.heigth > random.randint(0,200):
		tile.heigth -= 1
		for next in tile.next:
			next.heigth += 1/8
		if tile.heigth > 150:
			if random.randint(0,10) == 0:
				tile.RGB =  (1,1,1)
				tile.terrain = 'rock'
				tile.output = 0
	elif t > tile.heigth and tile.heigth < random.randint(-200,0):
		tile.heigth += 1
		for next in tile.next:
			next.heigth -= 1/8
			
def vegetation(start):
	if start.terrain == 'grass':
		density = 3
		openl = [ ]
		openl.append(start)
		for tile in openl:
			for n in tile.next:
				if n.terrain == "grass":
					if random.randint(0,2) < 2:
						if density == 3:
							openl.append(n)
							if random.randint(0,5) < 4:
								tile.terrain = 'woods'
								tile.output = 4
							else:
								if random.randint(0,5) < 2:
									tile.terrain = 'shrubs'
									tile.output = 5
								else:
									tile.terrain = 'grass'
						elif density == 2:
							openl.append(n)
							if random.randint(0,10) < 3:
								tile.terrain = 'shrubs'
								tile.output = 5
							else:
								tile.terrain = 'grass'
						elif density == 1:
							openl.append(n)
							if random.randint(0,50) < 50:
								tile.terrain = 'grass'
							else:
								tile.terrain = 'woods'
								tile.output = 4
						else:
							openl = [tile]
			if not density == 0:
				openl.remove(tile)
				if random.randint(0,3500/density**2) == random.randint(0,3500/density**2):
					density -= 1
					
	
def world_gen():
	global xlen
	global ylen
	xlen = 160
	ylen = 100
	global map_size
	map_size = [xlen,ylen]
	map = []
	Xsca = 1.1
	Ysca = 1.1
	Amp = 255
	Ampsca =  random.uniform(-.9,.9) + 1
	ss = perlin.SimplexNoise((random.randint(10000,100000))) #seed
	sm = perlin.SimplexNoise((random.randint(10000,100000))) #seed
	sb = perlin.SimplexNoise((random.randint(10000,100000))) #seed
	samp = perlin.SimplexNoise((random.randint(10000,100000))) #seed
	for x in range(0,xlen):
		map.append ([])
		for y in range (0,ylen):
			Amp = sb.noise2((x*Ampsca)/100,(y*Ampsca)/100)*200
			map[x].append(Tile(x,y,(sb.noise2(x*Xsca/100,y*Ysca/100)*Amp+sm.noise2(x*Xsca/10,y*Ysca/10)*Amp/2+ss.noise2(x*Xsca,y*Ysca)*Amp/4)))
	
	return map