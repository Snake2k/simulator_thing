# coding: utf-8
#This is the main file. Run this, to run the program
import random
import pygame
from pygame.locals import *
import math
import sys

import Menu
import Map
import Creatures
#Setup
pygame.init()
pygame.font.init()
#colors
black 	= (0	,0		,0)
red		= (255	,0		,0)
green	= (0	,255	,0)
yellow	= (255	,255	,0)
blue	= (0	,0		,255)
pink	= (255	,0		,255)
cyan	= (0	,255	,255)
white 	= (255	,255	,255)
# set up fonts
basicFont = pygame.font.SysFont(None, 48)

FPS = 600 # 60 frames per second
clock = pygame.time.Clock()

effects = []
global done
global buttons
global count
count = 0
done = False
font = pygame.font.SysFont("Calibri", 15)

#window
SWIDTH =  800
SHEIGTH = 600
global MainWindow
MainWindow = pygame.display.set_mode((SWIDTH, SHEIGTH))
def null():
	pass
	

def selmen(x):
	global buttons
	global Function
	global map
	global creat
	global carn
	if x == 0:
		MainWindow.fill(white)
		buttons = [Menu.Button(250,120,200,100,1,"Game")]
		Function = menu
	elif x == 1:
		buttons = [Menu.Button(0,500,200,100,0,"Menu")]
		print "Creating the map"
		map = Map.world_gen()
		print "Setting sea level"
		Map.set_sea(map)
		print "Creating general terrain info"
		for array in map:
			for tile in array:
				tile.set_tile()
		print "Erosion"
		for x in range(25):
			for array in map:
				for tile in array:
					Map.erosion(tile)
		from Map import xlen, ylen
		print "Planting forests"
		for x in range(5):
			tile =  map[random.randint(0,xlen-1)][random.randint(0,ylen-1)]
			Map.vegetation(tile)
		print "Painting the map"
		for array in map:
			for tile in array:
				Map.set_colour(tile)
		creat = []
		carn =  []
		print "creating fauna"
		for x in range(500):
			tile =  map[random.randint(0,xlen-1)][random.randint(0,ylen-1)]
			if not tile.terrain == 'water':
				creat.append(Creatures.Squwabble(tile,10,50))
		for x in range(20):
			tile =  map[random.randint(0,xlen-1)][random.randint(0,ylen-1)]
			if not tile.terrain == 'water':
				carn.append(Creatures.Braussian_Spider(tile,2,100))
		print "Done : ) \n \n"
		MainWindow.fill(white)
		for array in map:
			for tile in array:
				tile.draw()
		for creature in creat:
			creature.test()
		for creature in carn:
			creature.test()
		pygame.display.flip()
		Function = game
		
def menu():
	pygame.display.flip()

			
def game():
	global count
	count += 1
	if count == 60:
		print str(len(creat)) + " vs " + str(len(carn))
		for array in map:
			for tile in array:
				tile.step()
				count = 0
	if count%2 == 0:
		for creature in creat:
			creature.step()
	if count&1 == 0:
		for creature in carn:
			creature.step()
	
selmen(0)
while not done:
	for event in pygame.event.get(): # User did something
		if event.type == pygame.QUIT: # Closed from X
			done = True # Stop the Loop
		if event.type == pygame.MOUSEBUTTONUP:
			for button in buttons:
				button.pressed(pygame.mouse.get_pos())
	for button in buttons:
		button.draw()
	Function()
	clock.tick(FPS)
	pygame.display.set_caption("FPS: %i" % clock.get_fps())

sys.exit(0)